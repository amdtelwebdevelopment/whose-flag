# \<whose-flag\>

Match flags to their countries!

## Install the Polymer-CLI
First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) installed.
```
$ npm install -g polymer-cli
```
Next install dependencies with bower
```
$ bower install
```

## Install redux and redux-thunk
This app uses polymer-redux as a mixin but you will need to make sure that redux and redux-thunk are available.  While polymer2 still
uses bower as it's package manager you will need to use npm to install redux.
```
$ npm install --save redux redux-thunk
```
## Unzip the data folder
```
$ tar zxvf ./data.tar.gz
```

## Viewing Your Application

```
$ polymer serve
```

## Building Your Application

```
$ polymer build
```

This will create builds of your application in the `build/` directory, optimized to be served in production. You can then serve the built versions by giving `polymer serve` a folder to serve from:

```
$ polymer serve build/default
```

## Running Tests

```
$ polymer test
```

Your application is already set up to be tested via [web-component-tester](https://github.com/Polymer/web-component-tester). Run `polymer test` to run your application's test suite locally.
